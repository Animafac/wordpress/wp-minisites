��          t      �                 (     1  G   D     �     �     �     �  *   �     �  �       �     �     �  P     
   Y     d     v  
   �  *   �     �                           	                          
       Add a new mini website Animafac Animafac Minisites Custom post type used to integrate former mini-websites on animafac.net Mini website Mini website page Mini website pages Mini websites https://framagit.org/Animafac/wp-minisites https://www.animafac.net/ Project-Id-Version: Animafac Minisites 0.1.0
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/minisites
POT-Creation-Date: 2016-09-28 12:57:27+00:00
PO-Revision-Date: 2016-09-28 14:57+0100
Last-Translator: Pierre Rudloff <contact@rudloff.pro>
Language-Team: 
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.6.10
Plural-Forms: nplurals=2; plural=(n > 1);
 Ajouter un nouveau mini-site Animafac Mini-sites Animafac Custom post type utilisé pour intégrer les anciens mini-sites sur animafac.net Mini-sites Page de mini-site Pages de mini-site Mini-sites https://framagit.org/Animafac/wp-minisites https://www.animafac.net/ 