# Minisites

Custom post type used to integrate former mini-websites on animafac.net

## How it works

`Minisite::init()` adds a new `minisite_page` custom post type that can be grouped with the `minisite` taxonomy.
[Our WordPress theme](https://framagit.org/Animafac/wordpress/animafac-wp-theme) then displays this custom post type has a series of pages with a common TOC.
You can see an example [here](https://www.animafac.net/minisite/education-au-numerique/introduction/).

## Grunt tasks

[Grunt](https://gruntjs.com/) can be used to run some automated tasks defined in `Gruntfile.js`.

Your first need to install dependencies with [Yarn](https://yarnpkg.com/) and [Composer](https://getcomposer.org/):

```bash
yarn install
composer install
```

Be careful to not commit these dependencies in the repository!

You can remove them like this:

```bash
composer install --no-dev
```

(We can't add `vendor/` to `.gitignore` because we need to include autoload into the repository in case the plugin is installed with the regular WordPress installer and not Composer.)

### Lint

You can check that the JavaScript, JSON and PHP files are formatted correctly:

```bash
grunt lint
```

### Tests

You can run [PHPUnit](https://phpunit.de/) tests:

```bash
grunt test
```

### Translation template

You can update the POT translation template:

```bash
grunt makepot
```

## CI

[Gitlab CI](https://docs.gitlab.com/ee/ci/) is used to run the tests automatically after each commit.
