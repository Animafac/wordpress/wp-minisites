<?php
namespace Minisites\Test;

use Mockery;
use Minisites\Minisite;

class MinisiteTest extends \PHPUnit_Framework_TestCase
{
    protected function setUp()
    {
        \WP_Mock::setUp();
        \WP_Mock::wpFunction('register_post_type');
        \WP_Mock::wpFunction('__');
        \WP_Mock::wpFunction('register_taxonomy');
        Mockery::mock('overload:WP_Post');
        Mockery::mock('overload:WP_Term');
        $termMock = new \WP_Term();
        $termMock->slug = 'foo';
        \WP_Mock::wpFunction('wp_get_object_terms', ['return'=>[$termMock]]);
    }

    public function tearDown()
    {
        \WP_Mock::tearDown();
        \Mockery::close();
    }

    public function testInit()
    {
        Minisite::init();
    }

    /**
     * @dataProvider permalinkProvider
     */
    public function testPermalink($permalink, $result)
    {
        $postMock = new \WP_Post();
        $postMock->ID = 42;
        \WP_Mock::wpFunction('get_post', ['return'=>$postMock]);
        \WP_Mock::wpFunction('is_wp_error');
        $this->assertEquals(
            $result,
            Minisite::permalink(
                $permalink,
                42,
                false
            )
        );
    }

    public function permalinkProvider()
    {
        return [
            [
                'http://localhost/animafac/minisite/%minisite%/foobar/',
                'http://localhost/animafac/minisite/foo/foobar/'
            ],
            [
                'http://localhost/animafac/minisite/foobar/',
                'http://localhost/animafac/minisite/foobar/'
            ]
            ,
            [
                'http://localhost/animafac/minisite/%foobar%/foobar/',
                'http://localhost/animafac/minisite/%foobar%/foobar/'
            ]
        ];
    }

    public function testPermalinkWithoutPost()
    {
        \WP_Mock::wpFunction('get_post', ['return'=>false]);
        $this->assertEquals(
            'http://localhost/animafac/minisite/%minisite%/foobar/',
            Minisite::permalink(
                'http://localhost/animafac/minisite/%minisite%/foobar/',
                false,
                false
            )
        );
    }

    public function testPermalinkWithError()
    {
        $postMock = new \WP_Post();
        $postMock->ID = 42;
        \WP_Mock::wpFunction('get_post', ['return'=>$postMock]);
        \WP_Mock::wpFunction('is_wp_error', ['return'=>true]);
        $this->assertEquals(
            'http://localhost/animafac/minisite/%minisite%/foobar/',
            Minisite::permalink(
                'http://localhost/animafac/minisite/%minisite%/foobar/',
                false,
                false
            )
        );
    }
}
