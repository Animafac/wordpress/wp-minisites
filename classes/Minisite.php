<?php
/**
 * Minisite class.
 */

namespace Minisites;

/**
 * Main plugin class.
 */
class Minisite
{
    /**
     * Fires after WordPress has finished loading but before any headers are sent.
     * @return void
     * @see https://developer.wordpress.org/reference/hooks/init/
     */
    public static function init()
    {
        register_post_type(
            'minisite_page',
            array(
                'labels' => array(
                    'name' => __('Mini website pages', 'minisites'),
                    'singular_name' => __('Mini website page', 'minisites'),
                    'menu_name'=>__('Mini websites', 'minisites'),
                    'all_items'=>__('Mini website pages', 'minisites')
                ),
                'public' => true,
                'menu_icon' => 'dashicons-format-aside',
                'taxonomies' => ['themes'],
                'rewrite'=>['slug'=>'minisite/%minisite%'],
                'supports'=>['title', 'editor', 'page-attributes']
            )
        );
        register_taxonomy(
            'minisite',
            array('minisite_page'),
            array(
                'labels' => array(
                    'name' => __('Mini websites', 'minisites'),
                    'singular_name' => __('Mini website', 'minisites'),
                    'add_new_item'=>__('Add a new mini website', 'minisites')
                ),
                'show_admin_column'=>true,
                'sort'=>true
            )
        );
    }

    /**
     * Use a custom permalink for minisite pages.
     * @param  string $permalink Original permalink
     * @param  object $post_id
     * @param  bool   $leavename
     * @return string URL
     */
    public static function permalink($permalink, $post_id, $leavename)
    {
        if (strpos($permalink, '%minisite%') !== false) {
            $post = get_post($post_id);
            if ($post) {
                $terms = wp_get_object_terms($post->ID, 'minisite');
                if (!is_wp_error($terms) && !empty($terms) && is_object($terms[0])) {
                    return str_replace('%minisite%', $terms[0]->slug, $permalink);
                }
            }
        }
        return $permalink;
    }
}
