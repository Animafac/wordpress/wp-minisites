<?php
/**
 * Load dependencies needed by tests.
 */

require_once __DIR__ . '/vendor/autoload.php';
WP_Mock::bootstrap();
